/* Copyright (c) by Hugo Leisink <hugo@leisink.net>
 * This file is part of the Orb web desktop
 * https://gitlab.com/hsleisink/orb
 *
 * Licensed under the GPLv2 License
 */

function array_remove(arr, val) {
	arr = arr.filter(function(item) {
		return item !== val
	});

	return arr;
}

function url_encode(uri) {
	if (uri != undefined) {
		var parts = uri.split('/');

		parts.forEach(function(value, key) {
			value = encodeURIComponent(value);
			value = value.replace(/'/g, '%27');
			parts[key] = value;
		});

		uri = parts.join('/');
	}

	return uri;
}
